#!/bin/bash

function prompt(){

  echo "Do you wish to autodeploy mom_backend?"
  select yn in "Yes" "No"; do
      case $yn in
          Yes ) exec; break;;
          No ) exit;;
      esac
  done

}


# Init
function exec(){

  cur_dir=$(pwd)

  # Prepare
  set_hostname
  set_project_path

  # Add docker subnet for containers (172.20.1.0/24)
  add_subnetwork

  # Git deploy to folder
  git_deploy $directory

  # Ready to deploy
  deploy

}

# Set project hostname
function set_hostname(){

  echo "Specify the base domain. By default: momrline.local"
  read name

  if [ ! -n "$name" ]
  then
    hostname="momrline.local"
  else
    hostname="$name"
  fi

  # api /etc/hosts
  etc_hosts_add api."$hostname"

  # docs /etc/hosts
  etc_hosts_add docs."$hostname"

  # frontend web /etc/hosts
  etc_hosts_add web."$hostname"

  # Add hosts to nginx.conf
  generate_nginx_conf "$hostname"

  # API config edit
  set_configs "$hostname"
}

# Set project path
function set_project_path(){

  echo "Specify your projects directory. For example: ~/PhpstormProjects/redline"
  read directory

  if [ ! -z "$directory" ]
  then
    eval directory="$directory"

    if [ "$(ls -A "$directory")" ]
    then
      # Directory is not empty or not exists, retry
      echo The directory $directory is not empty
      set_project_path
    else
      directory="$directory"
    fi

  else
    echo "No root directory specified. Exiting.."
    exit;
  fi

  generate_docker_yml "$directory"

}


# Pull projects
function git_deploy(){

  deploy_path="$1"
  cd "$deploy_path"

  # API + DOCS
  api_url="git@gitlab.polexpert.us:rline/mom_backend.git"
  git clone "$api_url" mom_backend
  cd mom_backend
  git fetch origin
  git checkout dev
  cd ..

  # Web frontend
  web_url="git@gitlab.polexpert.us:rline/mom_frontend2.git"
  git clone "$web_url" mom_frontend
  cd mom_frontend
  git fetch origin
  git checkout develop
  cd ..

  # NodeJS websockets for notifications
  node_url="git@gitlab.polexpert.us:rline/mom_nodejs.git"
  git clone "$node_url" mom_nodejs

  # NodeJS websockets for chat
  chat_url="git@gitlab.polexpert.us:rline/chat-backend.git"
  git clone "$chat_url" mom_chat

  cd $cur_dir
}


# Set confif gor API
function set_configs(){

  host_name="$1"


  # Backend configs
  path=./backend/config
  cat $path/console-local.sample | sed -e 's|\#HOSTNAME\#|'$host_name'|g' > $path/console-local.php

  # Frontend
  path=./frontend/
  cat $path/settings.json.example | sed -e 's|\#HOSTNAME\#|'$host_name'|g' > $path/settings.json

}

# Add nginx host to hosts file
function etc_hosts_add(){

  # insert/update hosts entry
  ip_address="172.20.1.2"
  host_name="$1"

  # find existing instances in the host file and save the line numbers
  matches_in_hosts="$(grep -n $host_name /etc/hosts | cut -f1 -d:)"
  host_entry="${ip_address} ${host_name}"

  echo "Please enter your password if requested."

  if [ ! -z "$matches_in_hosts" ]
  then
      echo "Updating existing hosts entry."
      # iterate over the line numbers on which matches were found
      while read -r line_number; do
          # replace the text of each line with the desired host entry
          sudo sed -i '' "${line_number}s/.*/${host_entry} /" /etc/hosts
      done <<< "$matches_in_hosts"
  else
      echo "Adding new hosts entry."
      echo "$host_entry" | sudo tee -a /etc/hosts > /dev/null
  fi

}

# Add main host and docs to nginx file
function generate_nginx_conf(){

  host_name="$1"
  path=./nginx

  cat $path/yii.conf.example | sed -e 's|\#HOSTNAME\#|'$host_name'|g' > $path/yii.conf

}

# Add network subnet for our containers
function add_subnetwork(){

  subnet='app_subnet'
  exists=$(docker network ls | grep -E '(^| )'$subnet'( |$)' | wc -l)

  if [[ $exists > 0 ]]
  then
    echo "Subnetwork $subnet already exists"
  else
    echo "Subnetwork $subnet doesnt exists. Creating.."
    docker network create --gateway 172.20.1.1 --subnet 172.20.1.0/24 $subnet
  fi

}


# Generate docker-compose.yml
function generate_docker_yml(){

  uid=$(id -u)
  gid=$(id -g)

  path="$(echo -e "${1}" | tr -d '[[:space:]]')"
  cat docker-compose.example | sed -e 's|\#PATH\#|'$path'|g' |  sed -e 's|\#UID\#|'$uid'|g' | sed -e 's|\#GID\#|'$gid'|g' > docker-compose.yml

}

function deploy(){

  echo "Ready to deploy!"

  docker-compose up --build
  echo "It's done! Hit CTRL+C to de-attach and then run: docker-compose up -d"
}


prompt

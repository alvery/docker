#!/bin/bash

# Change redis server from localhost to redis
sed -i 's/localhost/redis/' ./config/config.js

mv /redline.help.crt ./config/cert/redline.help.crt
mv /redline.help.key ./config/cert/redline.help.key

npm install
npm start

var config = {};

//default
config.main = {};
config.redis = {};

config.cert = {};
config.main.port = 3555;
config.redis.host = 'redis';
config.redis.port = 6379;
config.redis.num_db = 10;
config.redis.subscribe_channel = "main_mom";
config.redis.keys = {
    userConfig : function(user_channel) {
        return 'user:' + user_channel + ':config';
    },
    iosDeviceToken : function(user_channel) {
        return 'user:' + user_channel + ':ios_device_token';
    },
    userProfile : function(user_channel) {
        return 'user:' + user_channel + ':profile';
    },
};

config.rabbitmq = {};
config.rabbitmq.host = 'amqp://127.0.0.1:5672';
config.rabbitmq.push_queue = 'push_queue';
config.rabbitmq.exchangeName = '';
config.rabbitmq.socketOptions = {};

// redefine config
switch (process.env.NODE_ENV) {
    case 'debug': //no break
    case 'development':
        config.cert.redline_crt = __base + 'config/cert/redline.help.crt';
        config.cert.redline_key = __base + 'config/cert/redline.help.key';
        break;

    case 'production':
        config.cert.redline_crt = __base + 'config/cert/redline.help.crt';
        config.cert.redline_key = __base + 'config/cert/redline.help.key';
        break;

    case 'local':
    default:
        config.cert.redline_crt = __base + 'config/cert/redline.help.crt';
        config.cert.redline_key = __base + 'config/cert/redline.help.key';
        break;
}

module.exports = config;

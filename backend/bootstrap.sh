#!/bin/bash

# Change permission for /runtime/logs
chmod 777 ./runtime
chmod 777 ./web/assets

# Composer install dependencies
composer global require "fxp/composer-asset-plugin:~1.1.1"
/usr/local/bin/composer install

# Rename config files
if [ ! -e ./config/elastic-settings-local.php ]
then
	cp ./config/elastic-settings-local.php.sample ./config/elastic-settings-local.php
	chmod 666 ./config/elastic-settings-local.php
fi

if [ ! -e ./config/params-local.php ]
then
	cp ./config/params-local.php.sample ./config/params-local.php
	chmod 666 ./config/params-local.php
fi

# Main config + Redis (Move from temp location)
if [ ! -e ./config/console-local.php ]
then
	mv /console-local.php ./config/console-local.php
	chmod 666 ./config/console-local.php
fi

if [ ! -e ./config/main-local.php ]
then
	mv /main-local.php ./config/main-local.php
	chmod 666 ./config/main-local.php
fi

if [ ! -e ./config/redis-connection-local.php ]
then
	mv /redis-connection-local.php ./config/redis-connection-local.php
	chmod 666 ./config/redis-connection-local.php
fi

# Hold on until available- just to be sure MySQL is ready to connect
until mysql -uroot -proot -hmysql; do
  >&2 echo "MySQL is unavailable - sleeping"
  sleep 1
done

# Start DB migrations
./yii migrate --interactive=0
./yii migrate --interactive=0 --migrationPath=@app/migrations/developer_data
./yii rbac/generate
./yii dev/init
./yii dev/test-data

# Generate api docs
./yii gendoc
